//
//  BeaconCell.swift
//  HeatMapStation
//
//  Created by Kevin Deem on 4/10/17.
//  Copyright © 2017 Kevin Deem. All rights reserved.
//

import UIKit

class BeaconCell : UITableViewCell {
  
  @IBOutlet weak var uuidLabel: UILabel!
  @IBOutlet weak var statsLabel: UILabel!
  
  
}
