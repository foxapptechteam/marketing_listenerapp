//
//  ViewController.swift
//  HeatMapStation
//
//  Created by Kevin Deem on 4/4/17.
//  Copyright © 2017 Kevin Deem. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource {
  
  var locationManager:CLLocationManager!
  var region:CLBeaconRegion!
  
  @IBOutlet weak var table: UITableView!
  var beaconData:Beacons?
  
  @IBOutlet weak var statsLabel: UILabel!
  
  var timer:Timer?
  
  let aUUID = "57FC53FA-7ADC-42A3-9FFB-9B7D8F9B57FA"
  let bUUID = "74278BDA-B644-4520-8F0C-720EAF059935"
  let cUUID = "e3520a1d-9c1a-435b-90ef-6e2bc1307496"
  
  let settings = UserDefaults.standard
  var goneToSettings = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    UserDefaults.standard.register(defaults: [String:Any]())
    if let uuid = UUID(uuidString: cUUID) {
      region = CLBeaconRegion(proximityUUID: uuid, identifier: "GuestBracelet")
      locationManager = CLLocationManager()
      locationManager.delegate = self
      locationManager.requestAlwaysAuthorization()
      beaconData = Beacons()
    } else {
      print("could not create UUID")
      abort()
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) && CLLocationManager.isRangingAvailable() {
      startScanning()
    }
    table?.delegate = self
    table?.dataSource = self
    startRefreshTimer()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    stopScanning()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  //MARK: - Timer Stuff
  func startRefreshTimer() {
    if timer != nil {
      return
    }
    timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: (#selector(self.refreshTable)), userInfo: nil, repeats: true)
  }
  
  func refreshTable() {
    guard let bData = beaconData else {
      return
    }
    table.reloadData()
    statsLabel.text = "\(bData.getStatusLabel())"
  }
  
  func stopRefreshTimer() {
    timer?.invalidate()
    timer = nil
  }
  
  // MARK: - location delegate
  
  
  func startScanning() {
    locationManager.startRangingBeacons(in: region)
    print("sent start command")
  }
  func stopScanning() {
    locationManager.stopRangingBeacons(in: region)
    print("sent stop command")
  }
  
  func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
    print("Monitoring start")
  }
  
  func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
    print("Monitoring failed to start \(error)")
  }
  
  func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
    guard let bData = beaconData else {
      return
    }
    bData.update(beacons)
    //bData.ageBeacons()
    //for beacon in beacons {
    //  bData.refresh(beacon: beacon)
    //}
    
    //bData.clearOldBeaconsOut()
    
    let identifier = UserDefaults.standard.string(forKey: "identifier_preference")
    if identifier == nil {
      if !goneToSettings {
        let alert = UIAlertController(title: "Need Identifier", message: "Please set the identifier for this base station (1 or 2)", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Open Settings", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
          guard let url = URL(string: UIApplicationOpenSettingsURLString) else {
            print("Failed to make URL for settings app")
            return
          }
          if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, completionHandler: { (success) in
              print("Opened settings \(success)")
              self.goneToSettings = true
            })
          }
          //exit(1)
        }))
        self.present(alert, animated: true, completion: nil)
      }
      
    } else {
      bData.sendBeaconReport()
    }
  }
  
  
  func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
    print("state is \(state)")
  }
  
  func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
    print("Entry event for \(region.identifier)")
  }
  
  func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
    print("Exit  event for \(region.identifier)")
  }
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) && CLLocationManager.isRangingAvailable() {
      print("Available")
      startScanning()
    } else {
      print("Failed at getting authorization \(status.rawValue)")
      stopScanning()
      
      let alert = UIAlertController(title: "Cannot Track Beacons", message: "Beacon detection Not Available", preferredStyle: UIAlertControllerStyle.alert)
      alert.addAction(UIAlertAction(title: "Quit", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
        exit(1)
      }))
      self.present(alert, animated: true, completion: nil)
    }
  }

  //MARK: - Data Source
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard let bData = beaconData else {
      return 0
    }
    return bData.beacons.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = table.dequeueReusableCell(withIdentifier: "UUIDCell", for: indexPath) as! BeaconCell
    guard let bData = beaconData else {
      return cell
    }
    let beacon = bData.beacons[indexPath.row]
    cell.uuidLabel.text = beacon.uuidLabelText
    cell.statsLabel.text = beacon.statsLabelText
    
    return cell
  }
}

class Beacons {
  
  var beacons:[Beacon] = []
  var transmitStatus = "N/A"
  
  func update(_ beacons:[CLBeacon]) {
    ageBeacons()
    var newList:[Beacon] = []
    var oldList = Array(self.beacons)
    for beacon in beacons {
      if let index = oldList.index(where: {(b) in b.isEqualTo(other:beacon)}) {
        let b = oldList.remove(at: index)
        b.hitFound(rssi:beacon.rssi, accuracy: beacon.accuracy)
        newList.append(b)
      } else {
        newList.append(Beacon(beacon:beacon))
      }
    }
    for beacon in oldList {
      if !beacon.isOut() {
        newList.append(beacon)
      }
    }
    newList.sort { (b1, b2) -> Bool in
      return b1.minor < b2.minor
    }
    self.beacons = newList
  }
  
  func ageBeacons() {
    for beacon in beacons {
      beacon.age()
    }
  }
  
  func sendBeaconReport() {
    sendBeaconReport(beacons:beacons)
  }
  
  func sendBeaconReport(beacons:[Beacon]) {
    guard let identifier = UserDefaults.standard.string(forKey: "identifier_preference") else {
      print("preference failure")
      
      return
    }
    var report:[String:Any] = [:]
    var beaconReports:[[String:Any]] = []
    report["listenerId"] = identifier as Any
    
    let beacons = self.beacons
    for beacon in beacons {
      let item:[String:Any] = [
      "UUID": beacon.uuid,
      "major": beacon.major,
      "minor": beacon.minor,
      "timestamp": Date().toMillis(),
      "rssi": beacon.rssi,
      "accuracy": beacon.accuracy]
      beaconReports += [item]
    }
    report["report"] = beaconReports
    do {
      let jsonData = try JSONSerialization.data(withJSONObject: report,options: JSONSerialization.WritingOptions())
      //let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
      //print("json string = \(jsonString)")
      
      let headers = [
        //        "x-api-key":"CrZyFl6VGu93TRQHtz9QA2paazCa19on2KsuXpYV",
        "cache-control":"no-cache"]
      
      if let url = URL(string: "https://9qiw4xcm88.execute-api.us-east-1.amazonaws.com/dev/reportbeacontraffic") {
        var req = URLRequest(url: url)
        req.httpMethod = "POST"
        req.cachePolicy = URLRequest.CachePolicy.useProtocolCachePolicy
        req.allHTTPHeaderFields = headers
        req.httpBody = jsonData
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: req, completionHandler:  {(dataParam, responseParam, error) in
          guard let data:Data = dataParam, let _:URLResponse = responseParam, error == nil else {
            print("Failed: \(error.debugDescription)")
            return
          }
          do {
            let statusData = try JSONSerialization.jsonObject(with: data) as! [String:Any]
            
            if let status = statusData["success"] {
              print("Success(\(String(describing:status))) - id:\(identifier), beacons: \(beacons.count)")
              self.transmitStatus = "OK"
            }
            else if let status = statusData["message"] {
              print("Failure:\(status)\nResponse:\(responseParam.debugDescription)\nEnd of response")
              self.transmitStatus = String(describing: status)
            } else {
              self.transmitStatus = "Unknown"
            }
            
 
          } catch {
            print("Failed deserializing")
          }
        })
        task.resume()
        
      }
      
    } catch {
      print("Failed")
    }
  }
  
  func getStatusLabel() -> String {
    if let id = UserDefaults.standard.string(forKey: "identifier_preference") {
      return "\(id)) \(beacons.count) beacons detected [\(transmitStatus)]"
    }
    else {
      return "\(beacons.count) beacons detected [\(transmitStatus)]"
    }
  }
  
  func contains(beacon:CLBeacon?) -> Bool {
    return beacons.contains(where: {(b:Beacon) in b.isEqualTo(other:beacon)})
  }
  
  func refresh(beacon:CLBeacon) {
    if let index = beacons.index(where: {(b) in b.isEqualTo(other:beacon)}) {
      let b = beacons[index]
      b.hitFound(rssi:beacon.rssi, accuracy: beacon.accuracy)
    } else {
      beacons += [Beacon(beacon:beacon)]
    }
  }
}

class Beacon {
  var uuid:String
  var major:Int
  var minor:Int
  var rssi:Int
  var accuracy:CLLocationAccuracy
  var strikes = 0
  let STRIKE_LIMIT = 0
  
  init(beacon:CLBeacon) {
    self.uuid = beacon.proximityUUID.uuidString
    self.major = beacon.major.intValue
    self.minor = beacon.minor.intValue
    self.rssi = beacon.rssi
    self.accuracy = beacon.accuracy
  }
  
  init(uuid:String, major:Int, minor:Int, rssi:Int, accuracy:CLLocationAccuracy) {
    self.uuid = uuid
    self.major = major
    self.minor = minor
    self.rssi = rssi
    self.accuracy = accuracy
  }
  
  var displayAccuracy:String {
    get {
      return String(format:"%.3f", accuracy)
    }
  }
  
  var uuidLabelText:String {
    get {
      return "\(uuid) (\(major),\(minor))"
    }
  }
  
  var statsLabelText:String {
    get {
      var rv = "rssi:\(rssi), accuracy:\(displayAccuracy)"
      if isStale {
        rv += " stale"
      }
      return rv
    }
  }
  
  var isStale:Bool {
    get {
      return strikes > 0
    }
  }
  
  func age() {
    strikes += 1
  }
  
  func hitFound(rssi:Int, accuracy:CLLocationAccuracy) {
    strikes = 0
    self.rssi = rssi
    self.accuracy = accuracy
  }
  
  func isOut() -> Bool {
    return strikes >= STRIKE_LIMIT
  }
  
  func isEqualTo(other:CLBeacon?) -> Bool {
    guard let o = other else {
      return false
    }
    return o.major.intValue == self.major && o.minor.intValue == self.minor
  }
}

extension Date {
  func toMillis() -> Int64! {
    return Int64(self.timeIntervalSince1970 * 1000)
  }
}
